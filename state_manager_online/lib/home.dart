import 'package:flutter/material.dart';
import 'package:state_manager_online/provider.dart';
// ignore: camel_case_types
class home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final bloc = provider.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('${bloc.getUserValue}'),
      ),
    );
  }

}