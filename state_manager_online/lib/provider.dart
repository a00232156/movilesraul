import 'package:flutter/material.dart';
import 'package:state_manager_online/Login_block.dart';


class provider extends InheritedWidget {


  final loginBlock = Login_block();

  provider.({Key key, Widget child})
    : super (key: key,child:child);
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static loginBlock of (BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<provider>().loginBlock;
  }

}