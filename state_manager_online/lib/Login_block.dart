import 'dart:async';

class Login_block {
  final _userNameController = BehaviorSubject<String>();

  Stream<String> get userNameStream => _userNameController.stream;
  //instructions to get values into the Stream
  get userName => _userNameController.sink.add;

  //obtener un valor del stream



  void dispose(){
    _userNameController.close();
  }

}