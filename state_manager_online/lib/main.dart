import 'package:flutter/material.dart';
import 'package:state_manager_online/login.dart';
import 'package:state_manager_online/provider.dart';
import 'package:state_manager_online/home.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'State Management',
        initialRoute: 'login',
        routes: {
          'login': (context) => login(),
          'home' : (context) => home(),
        },
      )
    );

  }

}