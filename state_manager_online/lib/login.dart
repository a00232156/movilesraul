import 'package:flutter/material.dart';
import 'package:state_manager_online/Login_block.dart';
import 'package:state_manager_online/provider.dart';


class login extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    final bloc = provider.of(context);



    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            buildTextField(bloc),
            SizedBox(height: 30.0,),
            FlatButton(
                onPressed: (){
                  Navigator.pushNamed(context, 'home');
                },
              child: Text('Press Me !'),

            ),
          ],
        ),
      ),
    )

  }

  Widget buildTextField(Login_block bloc) {
    return StreamBuilder(
      stream: bloc.userNameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TextField(
          decoration: InputDecoration(
              labelText: 'Enter username',
              counterText: snapshot.data,
          ),
          onChanged: (value) => bloc.userName(value),
        );
      },
    )



  }
  



}