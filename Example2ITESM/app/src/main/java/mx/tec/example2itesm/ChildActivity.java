package mx.tec.example2itesm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ChildActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button myButton;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        myButton = findViewById(R.id.button);
        myButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                /*myTextView.setText("You clicked me!");*/
                /*intento de la aplicación de iniciar una nueva actividad*/
                Intent myIntent = new Intent(ChildActivity.this,MainActivity.class);
                startActivity(myIntent);


            }
        });
    }
}